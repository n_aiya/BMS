package com.xj.common.base.common.exception;

/**
 * @author xj
 *
 */
public class Message  {

	// Message Code
	private String messageCode;
	// 相关参数，用于Format消息用
	private String[] params;
	// 消息具体内容，通常不需要设置，而应通过Code获取
	private String message;
	/**
	 * @return the messageCode
	 */
	public String getMessageCode() {
		return messageCode;
	}
	/**
	 * @param messageCode the messageCode to set
	 */
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	/**
	 * @return the params
	 */
	public String[] getParams() {
		return params;
	}
	/**
	 * @param params the params to set
	 */
	public void setParams(String[] params) {
		this.params = params;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}

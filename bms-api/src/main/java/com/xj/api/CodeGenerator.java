package com.xj.api;

import java.util.Collections;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
/**
 * <p>
 * 代码生成器演示
 * </p>
 */

public class CodeGenerator {
	
	public static void main(String[] args) {
		FastAutoGenerator.create("jdbc:mysql://127.0.0.1:3306/bms", "root", "123456")
	    .globalConfig(builder -> {
	        builder.author("xj") // 设置作者
	            .enableSwagger() // 开启 swagger 模式
	            .fileOverride() // 覆盖已生成文件
	            .outputDir("D://testcode"); // 指定输出目录
	    })
	    .packageConfig(builder -> {
	        builder.parent("com.xj.common.bussiness") // 设置父包名
	            .moduleName("wx") // 设置父包模块名
	            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "D://testcode")); // 设置mapperXml生成路径
	    })
	    .strategyConfig(builder -> {
	        builder.addInclude("wx_account") // 设置需要生成的表名
	            .addTablePrefix("wx_"); // 设置过滤表前缀
	    })
	    .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
	    .execute();
	} 
	
}
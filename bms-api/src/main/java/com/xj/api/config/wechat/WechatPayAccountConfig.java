package com.xj.api.config.wechat;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "wechat.pay")
public class WechatPayAccountConfig {
	
	private String appId;
	
	private String mchId;
	
	private String mchKey;
	
	private String subAppId;
	
	private String subMchId;
	
	private String keyPath;
	
}

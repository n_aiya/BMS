package com.xj.api.config.wechat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;

/**
 * 
 * @author Eric.Xu
 * 微信支付配置初始化
 */
@Configuration
@ConditionalOnClass(WxPayService.class)
@EnableConfigurationProperties(WechatPayAccountConfig.class)
public class WechaPayConfig{
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private WechatPayAccountConfig wechatPayAccountConfig;
	
	@Bean
	@ConditionalOnMissingBean
	public WxPayService wxPayService() {
		WxPayConfig payConfig = new WxPayConfig();
		payConfig.setAppId(wechatPayAccountConfig.getAppId());
	    payConfig.setMchId(wechatPayAccountConfig.getMchId());
	    payConfig.setMchKey(wechatPayAccountConfig.getMchKey());
	    payConfig.setSubAppId(wechatPayAccountConfig.getSubAppId());
	    payConfig.setSubMchId(wechatPayAccountConfig.getSubMchId());
	    payConfig.setKeyPath(wechatPayAccountConfig.getKeyPath());
	    // 可以指定是否使用沙箱环境
		payConfig.setUseSandboxEnv(false);
		WxPayService wxPayService = new WxPayServiceImpl();
		wxPayService.setConfig(payConfig);
		return wxPayService;
	}
	
}

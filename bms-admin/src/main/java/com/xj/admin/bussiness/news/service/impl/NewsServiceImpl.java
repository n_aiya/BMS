package com.xj.admin.bussiness.news.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xj.admin.bussiness.news.service.INewsService;
import com.xj.common.bussiness.news.entity.News;
import com.xj.common.bussiness.news.mapper.NewsMapper;

/**
 * <p>
 * 栏目管理 服务实现类
 * </p>
 *
 * @author xj
 * @since 2022-07-26
 */
@Service
public class NewsServiceImpl extends ServiceImpl<NewsMapper, News> implements INewsService {

}

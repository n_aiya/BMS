package com.xj.admin.bussiness.news.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xj.common.bussiness.news.entity.News;

/**
 * <p>
 * 栏目管理 服务类
 * </p>
 *
 * @author xj
 * @since 2022-07-26
 */
public interface INewsService extends IService<News> {

}

package com.xj.admin.base.dict.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xj.admin.base.dict.entity.TbDict;
import com.xj.admin.base.dict.mapper.TbDictMapper;
import com.xj.admin.base.dict.service.ITbDictService;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xj
 * @since 2017-06-01
 */
@Service
public class TbDictServiceImpl extends ServiceImpl<TbDictMapper, TbDict> implements ITbDictService {

	@Autowired
	private TbDictMapper dictMapper;
	
	@Override
	public Page<TbDict> selectDictPage(Page<TbDict> page, Wrapper<TbDict> wrapper) {
		page.setRecords(dictMapper.selectDictPage(page, wrapper));
		return page;
	}

	public Map<Integer, String> selectDictListByCode(String code) {
		Map<Integer,String> codeDicts = new HashMap<>();
		List<TbDict> dicts = dictMapper.selectList(new QueryWrapper<TbDict>().eq("code", code).ne("num", 0).orderByDesc("num"));
		for(TbDict dict:dicts){
			codeDicts.put(dict.getNum(), dict.getName());
		}
		return codeDicts;
	}

	
}

package com.xj.admin.base.userrole.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xj.admin.base.userrole.entity.TbRoleUser;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author xj
 * @since 2016-12-26
 */
public interface TbRoleUserMapper extends BaseMapper<TbRoleUser> {

}
package com.xj.admin.base.dict.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xj.admin.base.dict.entity.TbDict;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author xj
 * @since 2017-06-01
 */
public interface TbDictMapper extends BaseMapper<TbDict> {
	
	public List<TbDict> selectDictPage(Page<TbDict> page,@Param("ew") Wrapper<TbDict> wrapper);
	
}